import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Nav from "../components/nav";
import Title from "../components/title";
import {Table} from 'react-materialize';
import fire from '../fire';
//import react router
//import react materialize
//import cdn for materialize & jquery
//make pages and navigation
//form for 
export default class Notes extends Component {
  constructor(props) {
    super(props);
    this.state = { companies: [],datas:[] }; // <- set up react state
  }
  componentWillMount(){
    this.getDateInputs();
  }
  getDateInputs=()=>{
    /* Create reference to messages in Firebase Database */
  let companiesRef = fire.database().ref('companyName/');
  companiesRef.on("child_added", snapshot => {
    //look into foreach instead of on???
    /* Update React state when message is added at Firebase Database */
    let company = { text: snapshot.val(), id: snapshot.key };
    this.setState({ companies: [company].concat(this.state.companies) });
  })
}
alertBox = (e) =>{
    if (window.confirm("Would you like to delete the note for " + e.target.id+"?")) {
        //delete from db
          /* Send the message to Firebase */
          fire.database().ref(`/companyName/${e.target.id}`).set( null );
          fire.database().ref(`/contactName/${e.target.id}`).set( null );
          fire.database().ref(`/contactNumber/${e.target.id}`).set( null );
          fire.database().ref(`/generalNotes/${e.target.id}`).set( null );
          fire.database().ref(`/opinion/${e.target.id}`).set( null );
          fire.database().ref(`/interview/${e.target.id}`).set( null );
          // console.log("deleted");
        
    } else {
        //return(do nothing)
        return
    }
}
  render() {
    return (
      <div className="container">
        <Title title="Notes" showButton={true} buttonLink="/new-note"/>
        <Table hoverable={true}>
          <thead>
              <tr>
                <th className="center"data-field="id">Company Name</th>
              </tr>
          </thead>
          <tbody>
            {this.state.companies.map( (company,i) => 

            <tr key={company.id} className="center">
              <Link to='/edit-note'>
                <td onClick={this.props.onClick} key={company.id}>
                  {company.text}
                </td>
              </Link>
              <i id={company.text} onClick={this.alertBox}className="right material-icons">delete</i>
            </tr>
            )}
                                   {/* {JSON.stringify(this.state.datas, null, 2)} */}

          </tbody>
        </Table>
        <Nav/>
      </div>
    );
  }
}

