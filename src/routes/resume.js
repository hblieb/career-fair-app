import React, { Component } from 'react';
//import Router from 'react-router-dom';
import Nav from "../components/nav";
import Title from "../components/title";
import {Divider} from 'react-materialize';
import '../resume.css'
//import react router
//import react materialize
//import cdn for materialize & jquery
//make pages and navigation
//form for 
export default class Resume extends Component {
  render() {
// const divStyle={
//   padding:"15px 15px 15px 15px",
//   borderStyle: "solid"
// }
    return (
  <div className='container' >
   <Title title="Resume"/>
   <h3 className="center-align">Jordan Liebman</h3>
   <p className="center-align"><b>10 Port Way | Columbia, MO 65201 | (314)341-9895 | jordanliebman329@gmail.com | https://github.com/fudgefool98
</b></p>
      <h4><i className="fas fa-user-tie blue-text"></i> Technical and Related Experience</h4>
      <Divider />

   <div className='padding '>
      <p><b>Instant Quote Website in ReactJS for Internship, Missouri Employers Mutual, Columbia Mo <br />May 2018- August 2018</b></p>
        <li>Worked with React.js, to create custom components, and used imported components from NPM   such as React-Router-DOM, and Axios JS HTTP client, and React-Materialize.</li>
         <li>
            Used custom .NET CORE APIs as well as industry API Valen for Insurance analytical data. Gained experience with an Agile team in a professional setting.
           </li>
         <li>
           Deployed builds to Docker containers to test mobile and desktop design requirements.
           </li>
           <br />
      <Divider />
      <p><b>Sales Commission Calculator UWP (Universal Windows Platform), Missouri Employers Mutual, Columbia Mo<br />August 2018</b></p>
      <p>
         <li>Developed an application that calculated Partnership Level, Commission rates and Commission totals for aid in selling policies.</li>
         <li>Deployed as a Universal Windows Platform(UWP) Application to Windows 10 devices</li>
         <li>Used HTML Javascript, JQuery, and Materialize CSS.</li>
      </p>
      <Divider />
      <p><b>Website Fandomdb with  a LAMP stack (Linux, Apache, MySql, PHP), Capstone Project	<br />January 2018 - May 2018	
         </b>
      </p>
         <li>Lead a team of Senior Capstone IT Developers in the design and creation of a website and database.</li>
         <li>Created a relational database schema to hold users, their content, its fandom  and allow for creation of new fandoms.</li>
         <li>Tied a Mysql PHP backend to a HTML and Bootstrap css front end, to dynamically populate pages with content created by users.</li>
         <li>Maintained the DNS and Development servers configuration for development and deployment.</li>
       </div>
      <h4> <i className="fas fa-graduation-cap blue-text"></i> EDUCATION</h4>
      <Divider />

   <div className='padding '>
      <p><b>University of Missouri Columbia Bachelor of Science<br/>Expected Graduation Dec. 2018
         </b>
      </p>
      <ul>
         <li>Major: Information Technology</li>
         <li>Minor: Computer Science</li>
         <li>Major GPA: 3.6</li>
      </ul>
      <h5>
         Completed Coursework Includes: 
      </h5>
      <p>Capstone, WebDev(I,II), Object Oriented Programming(I,II), C# .NET, Database, Software Engineering, Algorithm Design(I,II), Cyber Security, Networking Technology, Statistics, Economics, Accounting, Entrepreneurship.</p>
      <Divider />
   </div>
      <h4><i className="fas fa-briefcase blue-text"></i> WORK EXPERIENCE</h4>
      <Divider />

   <div className='padding '>
      <p>
         Regularly employed since 2007. Responsible, trusted member of several work teams.
      </p>
      <p><b>
         Enterprise Architect Part Time, Missouri Employers Mutual, Columbia Mo<br/>	Aug 2018—Present
         </b>
      </p>
      <p>
         Develop and maintain applications in NodeJS ReactJS and Windows Universal Platform
         Maintain ReactJS Quote Website
      </p>
      <p><b>
         Enterprise Architect Intern , Missouri Employers Mutual, Columbia Mo<br/>	May 2018—Aug 2018
         </b>
      </p>
      <p>
         Created ReactJS Quote Website, and presented our product to the CIO.
      </p>
      <p><b>User Support, Bonfyre, St. Louis MO<br/>	Mar 2015—Aug 2015
         </b>
      </p>
      <p>
         Supported users of this group-messaging app during customer events. 
         Promoted the use of Bonfyre during conferences, conventions, etc.
      </p>
   </div>
      <h4><i className="fas fa-clipboard-list blue-text"></i> OTHER SKILLS & INFO</h4>
      <Divider />

   <div className='padding '>
         <li>Experience with Amazon Web Services, including API Gateway, S3 buckets, Ec2 instances, and lambda functions</li>
         <li>Solid understanding of Agile principles, and Scrum framework</li>
         <li>Solid understanding of Windows, Linux, and Unix operating systems</li>
         <li>Confident public speaker</li>
   </div>
   <Nav/>
</div>
    );
  }
}

