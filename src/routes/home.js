import React, { Component } from 'react';
// import Router from 'react-router-dom';
import Nav from "../components/nav";
import Title from "../components/title";
//import react router
import {Row,Col} from 'react-materialize'
import Divider from 'react-materialize/lib/Divider';
//import cdn for materialize & jquery
//make pages and navigation
//form for 
export default class Home extends Component {
  render() {
//const background = {
      // backgroundColor : 'rgb(175, 175, 175)',
      // position :"absolute", 
      // height :"100vh",
      // width: "100vw",
   // }
//     const text  = {
//       // textIndent : '3em',
//    padding:"2em",
// }
const weightName = {
  fontWeight: 250,
}
const weightLocation = {
  // fontWeight: 350,
}
    return (
<div className="container">
   <Title title="Welcome!"/>
   <Row>
      <Col l={1} className="">
      </Col>
      <Col l={10} className="">
     
      <Row>
      <Col l="2">
</Col>
         <Col l="10">
         <h2 style={weightName}>My name is Jordan,</h2>
         </Col>
      </Row>
         <br/>
         <br/>
      
      <Divider />
      <br/>
      <Row>
         <Col l={2}>
         <i className="fab fa-react fa-4x blue-text center center-align"></i> 
         </Col>
         <Col l={8}>
         <h5>I am an aspiring Software Engineer/Web Developer, most comfortable with Javascript and React.js, but willing to learn and loving every moment of it!</h5>
         <p>I have experince with C#, Java, Swift, PHP, SQL, and C from course work, and have played around with python and kotlin aswell</p>
         </Col>
      </Row>
      <Divider />
      <br/>
      <Row>
         <Col l={2}>
         <i className="fas fa-user-graduate fa-4x blue-text center center-align"></i>
         </Col>
         <Col l={8}>
         <h5 >
            University of Missouri Columbia. (December 18)
            <br />
            B.S. in Information Technology, and a minor in Computer Science.
         </h5>
         </Col>
      </Row>
      <Divider />
      <br/>
      <Row>
         <Col l={2}>
         <i className="fas fa-plane fa-4x blue-text center center-align"></i>
         </Col>
         <Col l={8}>
         
         <h4 style={weightLocation}>
            Moving to Denver!
         </h4>
         </Col>
      </Row>
      </Col>
      <Col l={1}>
      </Col>
   </Row>
   {/* not sure about this next line */}
   {/* I have a passion for leadership, problem solving, and creating beautiful and functional applications.
   I am looking for opportunities in software engineering, and web development. */}
   <Nav />
</div>
    );
  }
}


