import React, { Component } from 'react';
 import {Link} from 'react-router-dom';
import {Col,Row} from 'react-materialize';

//import cdn for materialize & jquery
//make pages and navigation
//form for 
export default class Nav extends Component {
  render() {
      const bar = {

            // height: "12.5vh",
            // overflow: "hidden",
            // backgroundColor: "#333",
            // position: "fixed",
            // bottom: "0",
          
      }
      const barItem = {
        // display: "block",
        color: "#f2f2f2",
        textAlign: "center",
        marginTop:"4.5vh",
        textDecoration: "none",
      }
    return (
    <div style={bar} className=" center center-align">
      <Row s={12} m={12} l={12}>
          <Col s={3} m={3} l={3}>
        <Link className ="green-text " style={barItem} to="/">
            <i className="material-icons">home</i>
            <p><b>Home</b></p>
        </Link>
          </Col>
          <Col s={3} m={3} l={3}>
        <Link className ="green-text " style={barItem} to="/resume">
            <i className="material-icons">insert_drive_file</i>
            <p><b>Resume</b></p>
        </Link>
          </Col>
          <Col s={3} m={3} l={3}>
        <Link className ="green-text " style={barItem} to="/notes">
            <i className="material-icons">notes</i>
            <p><b>Notes</b></p>
        </Link>
          </Col>
          <Col s={3} m={3} l={3}>
        <Link className ="green-text " style={barItem} to="/code">
            <i className="material-icons">code</i>
            <p><b>Code</b></p>
        </Link>
          </Col>
      </Row>

      </div>
    );
  }
}

