import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Row,Col} from 'react-materialize';
//import cdn for materialize & jquery
//make pages and navigation
//form for 
export default class Nav extends Component {
  render() {
      const divStyle = {
        // backgroundColor : 'grey',
        //   height: "12.5vh",
        //   textAlign:"center",
      }
      const h3Style = {
          fontWeight: 200,
      }
      
    return (
    <Row s={12}style={divStyle} className='valign-wrapper'>
          <Col s={10}>
          <h1 style={h3Style}>{this.props.title}</h1>
          </Col>
          <Col s={2}>
{          
  this.props.showButton?
  <Link  to={this.props.buttonLink}>
    <i className="center medium material-icons">add</i>
  </Link>
:""
}          </Col>
            
      </Row>
    );
  }
}

