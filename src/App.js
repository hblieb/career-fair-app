import React, { Component } from 'react';
import './App.css';
//routes
import Home from './routes/home';
import Resume from './routes/resume';
import Notes from './routes/notes'
import NewNote from './routes/new-note'
import EditNote from './routes/edit-note'
//componentes
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
//import Nav from './components/nav';
import Code from './routes/code';

//import cdn for materialize & jquery
//make pages and navigation
//form for 

class App extends Component {
  state = {
    fields: {}
  
  };
  
   onClick=(e)=>{
    this.setState({currentCompanyName: e.target.innerText});
   }
   reset=()=>{
     this.setState({});
   }
  render() {
    const homeWp = ()=>{
      return(
        <Home/>
      );
    }
    const resumeWp = ()=>{
      return(
        <Resume/>
      );
    }
    const notesWp = ()=>{
      return(
        <Notes onClick={this.onClick}/>
      );
    }
    const codeWp = ()=>{
      return(
        <Code/>
      );
    }
    const newNoteWp = ()=>{
      return(
        <NewNote />
      );
    }
    const editNoteWp = ()=>{
      return(
        <EditNote companyName={this.state.currentCompanyName} reset={this.reset}/>
      );
    }
     const background = {
    //   backgroundColor : 'rgb(175, 175, 175)',
    //   position :"absolute", 
    //   height :"100%",
    //   width: "100%",
     }
    return (
      <div className="App" style={background}>
        <div className="">
         <Router>
          <Switch >
            <Route exact path="/" render={homeWp} />
            <Route exact path="/resume" render={resumeWp} />
            <Route exact path="/notes" render={notesWp} />
            <Route exact path="/new-note" render={newNoteWp} />
            <Route exact path="/edit-note" render={editNoteWp} />
            <Route exact path="/code" render={codeWp} />
          
          </Switch>
         </Router>
        </div>
        {/* from app.js 
        {JSON.stringify(this.state, null, 2)} */}

      </div>
    );
  }
}

export default App;
